#!/bin/bash

echo "DECLARING VARIABLES"
declare -A artifacts

artifacts["uber-apk-signer.jar"]="patrickfav/uber-apk-signer uber-apk-signer .jar"

get_artifact_download_url()
{
    local api_url result
    api_url="https://api.github.com/repos/$1/releases/latest"
    result=$(curl -s $api_url | jq ".assets[] | select(.name | contains(\"$2\") and contains(\"$3\") and (contains(\".sig\") | not)) | .browser_download_url")
    echo "${result:1:-1}"
}

echo "CLEANING UP"
if [[ "$1" == "clean" ]]
    then
    rm -f uber-apk-signer.jar
    exit
fi

echo "FETCHING DEPENDENCIES"
for artifact in "${!artifacts[@]}"
do
    if [ ! -f "$artifact" ]
    then
        echo "Downloading $artifact"
        curl -sLo "$artifact" $(get_artifact_download_url ${artifacts[$artifact]})
    fi
done

echo "PREPARING"
mkdir -p youtube/output/release
mkdir -p twitter/output/release
mkdir -p reddit/output/release
mkdir -p tiktok/output/release
mkdir -p yt.music.v7a/output/release
mkdir -p yt.music.v8a/output/release
mkdir -p latest/app

echo "SIGNING PACKAGES"
if [ -f youtube/output/"youtube.apk" ]
then
    java -jar uber-apk-signer.jar --allowResign -a youtube/output -o youtube/output/release
    mv -v youtube/output/release/youtube-aligned-debugSigned.apk latest/app/youtube.apk
    mv -v youtube/micro.g.apk latest/app/micro.g.apk

    java -jar uber-apk-signer.jar --allowResign -a twitter/output -o twitter/output/release
    mv -v twitter/output/release/twitter-aligned-debugSigned.apk latest/app/twitter.apk

    java -jar uber-apk-signer.jar --allowResign -a reddit/output -o reddit/output/release
    mv -v reddit/output/release/reddit-aligned-debugSigned.apk latest/app/reddit.apk

    java -jar uber-apk-signer.jar --allowResign -a tiktok/output -o tiktok/output/release
    mv -v tiktok/output/release/tiktok-aligned-debugSigned.apk latest/app/tiktok.apk

    java -jar uber-apk-signer.jar --allowResign -a yt.music.v7a/output -o yt.music.v7a/output/release
    mv -v yt.music.v7a/output/release/yt.music.v7a-aligned-debugSigned.apk latest/app/yt.music.v7a.apk

    java -jar uber-apk-signer.jar --allowResign -a yt.music.v8a/output -o yt.music.v8a/output/release
    mv -v yt.music.v8a/output/release/yt.music.v8a-aligned-debugSigned.apk latest/app/yt.music.v8a.apk
fi

echo "DONE SIGNING"
