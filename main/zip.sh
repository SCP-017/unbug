#!/bin/bash

#### 7z Script

echo "SET DATE"
date_now=$(date +'%B-%d')
RANDOM=$$

echo "MAKING DIRECTORY"
mkdir -p zip

echo "ZIPPING FILES"
7z a -pJUNE122019 -mx=9 -t7z zip/$date_now.$RANDOM.youtube.7z latest/app/micro.g.apk latest/app/youtube.apk latest/app/yt.music.v7a.apk latest/app/yt.music.v8a.apk
7z a -pJUNE122019 -mx=9 -t7z zip/$date_now.$RANDOM.socials.7z latest/app/reddit.apk latest/app/tiktok.apk latest/app/twitter.apk
