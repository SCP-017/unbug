import requests

##################################

#### EXE.io Script

#### dropgalaxy.com strings
dg_url = "https://www.file-upload.com"
dg_all_path = "urlpath"
dg_yt_path = "urlpath"
dg_soc_path = "urlpath"

#### exe.io strings
exe_io = "https://exe.io"
exe_api_key = "01dc0ae80090c28d0f0c7d57186ee6c6c13719e7"

#### Get DropGalaxy URL string.
url1 = "{}/{}".format(dg_url, dg_all_path)
url2 = "{}/{}".format(dg_url, dg_yt_path)
url3 = "{}/{}".format(dg_url, dg_soc_path)

#### Set URL string to exe.io short URL.
link1 = "{}/api?api={}&url={}&format=text".format(exe_io, exe_api_key, url1)
link2 = "{}/api?api={}&url={}&format=text".format(exe_io, exe_api_key, url2)
link3 = "{}/api?api={}&url={}&format=text".format(exe_io, exe_api_key, url3)

#### Get GitHub URL string to exe.io short URL string.
a = requests.get(link1)
b = requests.get(link2)
c = requests.get(link3)

#### Print ouo.io strings to terminal.
print("EXE.io Shortlinks")
print('all.7z : ' + a.text)
print('youtube.7z : ' + b.text)
print('socials.7z : ' + c.text)
