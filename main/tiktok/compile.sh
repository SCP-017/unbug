#!/bin/bash

patches_file=./tiktok.patch

included_start="$(grep -n -m1 'INCLUDED PATCHES' "$patches_file" | cut -d':' -f1)"
excluded_start="$(grep -n -m1 'EXCLUDED PATCHES' "$patches_file" | cut -d':' -f1)"

included_patches="$(tail -n +$included_start $patches_file | head -n "$(( excluded_start - included_start ))" | grep '^[^#[:blank:]]')"
excluded_patches="$(tail -n +$excluded_start $patches_file | grep '^[^#[:blank:]]')"

###################################

echo "DECLARING VARIABLES"
declare -A artifacts

artifacts["revanced-cli.jar"]="revanced/revanced-cli revanced-cli .jar"
artifacts["revanced-patches.jar"]="revanced/revanced-patches revanced-patches .jar"

###################################

get_artifact_download_url()
{
    local api_url result
    api_url="https://api.github.com/repos/$1/releases/latest"
    result=$(curl -s $api_url | jq ".assets[] | select(.name | contains(\"$2\") and contains(\"$3\") and (contains(\".sig\") | not)) | .browser_download_url")
    echo "${result:1:-1}"
}

###################################

populate_patches()
{
    while read -r revanced_patches
    do
        patches+=("$1 $revanced_patches")
    done <<< "$2"
}

###################################

echo "CLEANING UP"
if [[ "$1" == "clean" ]]
    then
    rm -f revanced-cli.jar revanced-patches.jar
    exit
fi

###################################

echo "SET EXPERIMENTAL"
if [[ "$1" == "experimental" ]]; then
    EXPERIMENTAL="--experimental"
fi

###################################

echo "FETCHING DEPENDENCIES"
for artifact in "${!artifacts[@]}"
do
    if [ ! -f "$artifact" ]
    then
        echo "DOWNLOADING $artifact"
        curl -sLo "$artifact" $(get_artifact_download_url ${artifacts[$artifact]})
    fi
done

###################################

echo "CALL POPULATE PATCHES"
[[ ! -z "$included_patches" ]] && populate_patches "-i" "$included_patches"
[[ ! -z "$excluded_patches" ]] && populate_patches "-e" "$excluded_patches"

###################################

echo "MAKING DIRECTORY"
mkdir -p output

###################################

echo "COMPILING TIKTOK"
if [ -f "com.zhiliaoapp.musically.apk" ]
then
    echo "COMPILING TIKTOK"
    java -jar revanced-cli.jar -b revanced-patches.jar -r \
        ${patches[@]} \
        $EXPERIMENTAL \
        -a com.zhiliaoapp.musically.apk -o output/tiktok.apk
else
    echo "NO BASE PACKAGE, SKIP COMPILING TIKTOK"
fi

echo "DONE"
