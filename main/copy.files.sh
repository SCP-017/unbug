#!/bin/bash

echo "MAKING DIRECTORIES"
mkdir -p reddit/version
mkdir -p tiktok/version
mkdir -p twitter/version
mkdir -p youtube/version
mkdir -p yt.music.v7a/version
mkdir -p yt.music.v8a/version

echo "COPYING VERSION FILES"
cp -p -v version.json reddit/version
cp -p -v version.json tiktok/version
cp -p -v version.json twitter/version
cp -p -v version.json youtube/version
cp -p -v version.json yt.music.v7a/version
cp -p -v version.json yt.music.v8a/version

echo "COPYING PATCH FILES"
cp -p -v patches/reddit.patch reddit
cp -p -v patches/tiktok.patch tiktok
cp -p -v patches/twitter.patch twitter
cp -p -v patches/youtube.patch youtube
cp -p -v patches/music.patch yt.music.v7a
cp -p -v patches/music.patch yt.music.v8a

echo "DONE COPYING FILES"
