# © ReVanced 2022

1. Check applications version.
2. Check ReVanced Patches.
3. Build ReVanced applications.
4. Check and release the draft.
5. Update github_tag in shorten.py.
6. Shorten the release links.
7. Copy the exe.io links in terminal.
8. Copy and update links on GitHub Pages.
